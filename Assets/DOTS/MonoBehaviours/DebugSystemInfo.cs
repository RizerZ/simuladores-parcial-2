﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugSystemInfo : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Procesador: "+ SystemInfo.processorType);
        Debug.Log("Número de procesadores: "+ SystemInfo.processorCount);
        Debug.Log("Frecuencia del procesador: "+ SystemInfo.processorFrequency);
        Debug.Log("Placa de video: "+ SystemInfo.graphicsDeviceName);
        Debug.Log("Memoria de video: " + SystemInfo.graphicsMemorySize);
        Debug.Log("Memoria RAM: "+ SystemInfo.systemMemorySize);
    }
}
