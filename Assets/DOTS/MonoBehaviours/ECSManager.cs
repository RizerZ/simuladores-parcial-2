﻿using UnityEngine;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Transforms;

public class ECSManager : MonoBehaviour
{
    public static EntityManager manager;
    public GameObject enemyPrefab;
    public GameObject whitePrefab;
    public GameObject redPrefab;
    public GameObject bulletPrefab;
    public GameObject player;
    int numEnemys = 4000;
    int numRed = 2000;
    int numBullet = 30;
    BlobAssetStore store;

    Entity bullet;

    public static Entity white;
    // Start is called before the first frame update
    void Start()
    {
        store = new BlobAssetStore();
        manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, store);
        Entity enemy = GameObjectConversionUtility.ConvertGameObjectHierarchy(enemyPrefab, settings);
        Entity red = GameObjectConversionUtility.ConvertGameObjectHierarchy(redPrefab, settings);
        bullet = GameObjectConversionUtility.ConvertGameObjectHierarchy(bulletPrefab, settings);
        white = GameObjectConversionUtility.ConvertGameObjectHierarchy(whitePrefab, settings);

        for (int i = 0; i < numEnemys; i++)
        {
            var instance = manager.Instantiate(enemy);
            float x = UnityEngine.Random.Range(-50, 50);
            float y = UnityEngine.Random.Range(-50, 50);
            float z = UnityEngine.Random.Range(-50, 50);

            float3 position = new float3(x, y, z);
            manager.SetComponentData(instance, new Translation { Value = position });

            float rspeed = UnityEngine.Random.Range(0.1f, 5f)/5f;
            manager.SetComponentData(instance, new EnemySpeedComponent { speed = rspeed });
        }

        for (int i = 0; i < numRed; i++)
        {
            var instance = manager.Instantiate(red);
            float x = UnityEngine.Random.Range(-50, 50);
            float y = UnityEngine.Random.Range(-50, 50);
            float z = UnityEngine.Random.Range(-50, 50);

            float3 position = new float3(x, y, z);
            manager.SetComponentData(instance, new Translation { Value = position });

            float rspeed = UnityEngine.Random.Range(0.1f, 5f);
            manager.SetComponentData(instance, new EnemySpeedComponent { speed = rspeed });
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            for(int i=0; i<numBullet;i++)
            {
                var instance = manager.Instantiate(bullet);
                var startPos = player.transform.position + UnityEngine.Random.insideUnitSphere * 2;
                manager.SetComponentData(instance, new Translation { Value = startPos });
                manager.SetComponentData(instance, new Rotation { Value = player.transform.rotation });
            }
        }
    }

    private void OnDestroy()
    {
        store.Dispose();
    }
}
