﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct EnemyComponent : IComponentData
{
    public bool alive;
}
