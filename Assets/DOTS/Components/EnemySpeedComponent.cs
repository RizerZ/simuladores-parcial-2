﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct EnemySpeedComponent : IComponentData
{
    public float speed;
}