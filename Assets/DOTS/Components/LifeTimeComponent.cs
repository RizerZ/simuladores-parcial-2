﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct LifeTimeComponent : IComponentData
{
    public float time;
}
