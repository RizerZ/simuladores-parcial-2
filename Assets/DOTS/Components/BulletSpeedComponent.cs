﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct BulletSpeedComponent : IComponentData
{
    public float speed;
}