﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Collections;
using Unity.Physics;
using UnityEngine;

public class DestroySystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float dT = Time.DeltaTime;

        Entities
            .WithoutBurst().WithStructuralChanges()
            .ForEach((Entity entity,
                        ref Translation position,
                        ref LifeTimeComponent ltc) =>
            {
                ltc.time -= dT;
                if (ltc.time <= 0f)
                {
                    EntityManager.DestroyEntity(entity);
                }

            }).Run();

        Entities
           .WithoutBurst().WithStructuralChanges()
           .ForEach((Entity entity,
                       ref Translation position,
                       ref EnemyComponent ec) =>
           {
               if (!ec.alive)
               {
                   for (int i = 0; i < 500; i++)
                   {
                       float3 offset = (float3)UnityEngine.Random.insideUnitSphere * 2f;
                       var instance = ECSManager.manager.Instantiate(ECSManager.white);
                       float3 randomDir = new float3(UnityEngine.Random.Range(-1, 1), UnityEngine.Random.Range(-1, 1), UnityEngine.Random.Range(-1, 1));
                       ECSManager.manager.SetComponentData(instance, new Translation { Value = position.Value + offset });
                       ECSManager.manager.SetComponentData(instance, new PhysicsVelocity { Linear = randomDir * 1.5f });
                   }

                   EntityManager.DestroyEntity(entity);
               }
           }).Run();

        return inputDeps;
    }
}
