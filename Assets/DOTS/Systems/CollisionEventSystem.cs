﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Unity.Physics;
using Unity.Physics.Systems;

[UpdateAfter(typeof(EndFramePhysicsSystem))]
public class CollisionEventSystem : JobComponentSystem
{
    BuildPhysicsWorld m_BuildphysicsWorldSystem;
    StepPhysicsWorld m_StepPhysicsWorldSystem;

    protected override void OnCreate()
    {
        m_BuildphysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
        m_StepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld>();
    }
    struct CollisionEventImpuseJob : ICollisionEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<BulletSpeedComponent> BulletGroup;
        public ComponentDataFromEntity<EnemyComponent> EnemyGroup;
        public void Execute(CollisionEvent collisionEvent)
        {
            Entity entityA = collisionEvent.Entities.EntityA;
            Entity entityB = collisionEvent.Entities.EntityB;

            bool isTargetA = EnemyGroup.Exists(entityA);
            bool isTargetB = EnemyGroup.Exists(entityB);

            bool isBulletA = BulletGroup.Exists(entityA);
            bool isBulletB = BulletGroup.Exists(entityB);

            if (isBulletA && isTargetB)
            {
                var aliveComponent = EnemyGroup[entityB];
                aliveComponent.alive = false;
                EnemyGroup[entityB] = aliveComponent;
            }

            if (isBulletB && isTargetA)
            {
                var aliveComponent = EnemyGroup[entityA];
                aliveComponent.alive = false;
                EnemyGroup[entityA] = aliveComponent;
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        JobHandle jobHandle = new CollisionEventImpuseJob
        {
            BulletGroup = GetComponentDataFromEntity<BulletSpeedComponent>(),
            EnemyGroup = GetComponentDataFromEntity<EnemyComponent>()
        }.Schedule(m_StepPhysicsWorldSystem.Simulation, ref m_BuildphysicsWorldSystem.PhysicsWorld, inputDeps);

        jobHandle.Complete();
        return jobHandle;
    }
}

