﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Collections;
using Unity.Physics;

public class BulletMovementSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float dT = Time.DeltaTime;

        var jobHandle = Entities
            .WithName("BulletMovementSystem")
            .ForEach((ref PhysicsVelocity physics,
                        ref Translation position,
                        ref Rotation rotation,
                        ref BulletSpeedComponent bsc) =>
            {
                physics.Angular = float3.zero;
                physics.Linear += dT * bsc.speed * math.forward(rotation.Value);
            }).Schedule(inputDeps);

        jobHandle.Complete();

        return jobHandle;
    }
}
